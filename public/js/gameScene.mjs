import * as PIXI from "./lib/pixi.mjs";
import {app, game, texture, createButton, createInput, createText, textStyleQuestion, findObject, delObject, setASP} from "./gameHelper.mjs"
import {clientConnGame,clientReturnAnswer,clientReturnParts,clientReturnVote,clientStartGame} from "./gameSocket.mjs"

export const scene={
    current:undefined,
    names:["login","lobby","gameQuestion","wait","gamePart","gameVote","gameResults","miniGame" /*TODO*/]
}

export function changeScene(newScene,force=false) {
    if(scene.current==newScene&&!force)
        return;
    app.stage.removeChildren()
    let thisScene = new PIXI.Container(), obj;
    thisScene.interactive = true;
    thisScene.hitArea = new PIXI.Rectangle(0, 0, game.w(), game.h())

    obj = createText(thisScene,'',game.w()*.5,game.h()*.1,new PIXI.TextStyle({fontSize:'6em',fill:0xff0000}))
    obj.name = "errorMsg"


    switch(newScene) {
        case "login":
            obj = createInput(thisScene,'Name',game.w()*.5,game.h()*.25,"uname")
            createInput(thisScene,'Room code',game.w()*.5,game.h()*.25 + obj.height*1.5,"code")

            createButton(thisScene,0,"Create game",game.w()*.5,game.h()*.6,()=>{clientConnGame()})
            createButton(thisScene,0,"Join game",game.w()*.5,game.h()*.8,()=>{clientConnGame(false)})

        break;
        case "lobby":
            createButton(thisScene,0,"Start game",game.w()*.5,game.h()*.8,()=>{clientStartGame()})
            console.log(game.players)
            for(let p of game.players) {
                obj = new PIXI.Sprite(texture.characters[p.ind])
                setASP(obj,0.5,2,p.lobbyPos[0]*game.w(),p.lobbyPos[1]*game.h())

                thisScene.addChild(obj)
                console.log(thisScene)
                console.log(texture)
                createText(obj,p.name,0,0)
            }
        
        break;
        case "gamePart":
        case "gameQuestion":
            const diffs = {"gamePart":[
                    "Write the following: " + game.parts[game.partsVal.length],
                    "It will be...",
                    ()=>{clientReturnParts()}
                ],"gameQuestion":[
                    game.questions[game.returnAnswers.length]?.text,
                    "My answer is..",
                    ()=>{clientReturnAnswer()}
                ]
            }
            
            obj = createText(thisScene,diffs[newScene][0],game.w()*.5,game.h()*.15,textStyleQuestion)
            obj.name="questionText"
            thisScene.addChild(obj)

            createInput(thisScene,diffs[newScene][1],game.w()*.5,game.h()*.6,"answer",true)
            createButton(thisScene,0,"Send",game.w()*.5,game.h()*.8,diffs[newScene][2])
            if(newScene=="gameQuestion") {
                const currentQuestion = game.questions[game.returnAnswers.length]
                game.partSelected=-1;
                console.log(game)
                for(let ind=0;ind<currentQuestion.parts.length;ind++) {
                    const bt = createButton(thisScene,0,currentQuestion.parts[ind],(.25+(ind%4)*.25)*game.w(),(.3+~~(ind/4)*.15)*game.h(),function(){
                        console.log(this)
                        let qT = findObject("questionText")
                        qT.text = qT.text.toString().replace("*",this.getChildAt(0).text)
                        game.partSelected=ind
                        for(let i=0;i<currentQuestion.parts.length;i++)
                            delObject("part"+i)
                    })
                    bt.name="part"+ind
                }
            }
            break;
        case "wait":
            thisScene.addChild(
                createText(thisScene,"Wait for the others..",game.w()*.5,game.h()*.15,textStyleQuestion))

            break;
        case "gameVote":
            createText(thisScene,game.question,game.w()*.5,game.h()*.25,textStyleQuestion)
            for(let ind=0;ind<game.answers.length;ind++) {
                createButton(thisScene,0,game.answers[ind],(.25+(ind%4)*.25)*game.w(),(.3+~~(ind/4)*.15)*game.h(),()=>{clientReturnVote(ind)})
            }
            break;
        case "gameResults":
            for(let p=0;p<game.players.length;p++) {
                const player = game.players[p]
                obj = new PIXI.Sprite(texture.characters[player.ind])
                setASP(obj,0.5,2,.2*game.w(),(.1+p/10)*game.h())
                createText(obj,player.score,texture.characters[0].width,0)

                thisScene.addChild(obj)
            }
            break;

        case "miniGame":
            for(let p of game.players) {
                obj = new PIXI.Sprite(texture.characters[p.ind])
                setASP(obj,0.5,2,game.w()*.5,game.h()*.5)

                thisScene.addChild(p)
            }
            break;
    }
    scene.current=newScene
    app.stage.addChild(thisScene)
}