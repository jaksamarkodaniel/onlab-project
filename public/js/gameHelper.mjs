import * as PIXI from "./lib/pixi_min.mjs";
import {TextInput} from "./lib/pixiTextInput.js"

const appSettings = {
    backgroundAlpha:0.2,
    roundPixels: true,
    resolution: window.devicePixelRatio || 1,
    autoResize: false
}

export const game = {
    w:()=>{
        if(getOrientation("portrait"))
            return 960;
        return 1920;
    },
    h:()=>{
        if(getOrientation("portrait"))
            return 1920
        return 1080
    },
    orientation: "",
    canInteract:true,
    
    question:"",
    voteN:0,
    questions:[],
    parts:[],
    partsVal:[],
    returnAnswers:[]
};


export const app = new PIXI.Application(appSettings),
    texture = {buttons:[],characters:[]},
    textStyleQuestion = new PIXI.TextStyle({
        fontFamily: 'Comic Sans',
        fontSize: 30,
        fontWeight: 'bold',
        lineJoin: 'round'
    });

export function setError(text) {
    let obj = findObject("errorMsg")

    obj.text=text
    obj.alpha=5
}

function getScene() {
    return app.stage.getChildAt(0)
}

export function getOrientation(isType=undefined) {
    const val = ((screen.orientation || {}).type || screen.mozOrientation || screen.msOrientation) || "";
    if(!isType)
        return val
    return val.indexOf(isType)!=-1
}

export function findObject(id) {
    if(app.stage.children.length==0)
        return undefined
    return getScene().getChildByName(id)
}

export function delObject(id) {
    if(app.stage.children.length==0)
        return undefined
    getScene().removeChild(findObject(id))
}

export function createInput(thisScene,str,x,y,name,autoFocus=false) {
    const input = new TextInput({
		input: {
			fontSize: '30px',
			padding: '12px',
			width: '500px',
			color: '#26272E'
		},
		box: {
			default: {fill: 0xE8E9F3, rounded: 12, stroke: {color: 0xCBCEE0, width: 3}},
			focused: {fill: 0xE1E3EE, rounded: 12, stroke: {color: 0xABAFC6, width: 3}},
			disabled: {fill: 0xDBDBDB, rounded: 12}
		}
	})
	
	input.placeholder = str

    input.name=name
    
	input.x = x
	input.y = y
	input.pivot.x = input.width/2
	input.pivot.y = input.height/2
    input.on('keydown',function(keycode){
        if(keycode==13) {
            this.blur()
        }
    })
    input.scale.set(1.25)
    if(getOrientation("portrait"))
        input.scale.set(1.5)
    thisScene.addChild(input)
    if(autoFocus)
        input.focus()
    return input
}

export function createText(thisScene, str,x,y,style=undefined) {
    const obj = setASP(new PIXI.Text(str,style),0.5,1,x,y)
    thisScene?.addChild(obj)
    return obj;
}

export function createButton(scene, texInd, textStr, x,y, clickFn) {
    let obj = new PIXI.Sprite(texture.buttons[texInd])

    obj.on('pointertap', clickFn)

    createText(obj,textStr,0,0,textStyleQuestion)

    setASP(obj,0.5,1.5,x,y)

    obj.interactive = true;
    obj.buttonMode = true;

    //obj.addChild(text)
    scene.addChild(obj)

    return obj;
}

export function setASP(obj,anchor,scale,x,y) {
    obj.anchor.set(anchor)
    obj.scale.set(scale)
    obj.position.set(x,y)

    return obj;
}