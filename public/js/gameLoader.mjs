import * as PIXI from "./lib/pixi.mjs";
import {texture} from "./gameHelper.mjs"

const loader = PIXI.Loader.shared;
const buttonN = 1
export function loadTextures(fn) {
    /*for(let i=1;i<=buttonN;i++)
        texture.buttons.push(PIXI.Texture.from('assets/button'+i+'.png'));*/

    for(let i=1;i<=buttonN;i++)
        loader.add('button'+i,'assets/button'+i+'.png')
    for(let i=0;i<6;i++)
        loader.add('character'+i,'assets/character'+i+'.png')

    loader.load((loader, resources) => {
        for(let i=1;i<=buttonN;i++)
            texture.buttons.push(resources["button"+i].texture)
        for(let i=0;i<6;i++)
            texture.characters.push(resources["character"+i].texture)
    })

    loader.onComplete.add(fn);
}