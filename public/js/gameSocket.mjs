import { io } from "./lib/socket.io.min.js";
import {findObject,setError} from "./gameHelper.mjs"
import {game} from "./gameHelper.mjs"
import {changeScene} from "./gameScene.mjs"

let sock;
export function clientConnGame(create=true) {
    sock = io('',{query:{
        "name": findObject("uname").text,
        "type": "Player",
        "connType": create ? "Create" : "Join",
        "code": findObject("code").text || ""
    }})
    sock.on("connect", () => {
        console.log(sock.id)
    });
    

    sock.on('sendPlayers',(players)=>{
        game.players=players
        changeScene("lobby",true)
    })

    sock.on('sendQuestions', (questions)=>{
        game.questions=questions;

        console.log(questions)

        changeScene("gameQuestion")
    })

    sock.on('sendParts',(parts)=>{
        game.parts=parts
        game.partsVal=[]

        console.log("Send parts:"+parts)

        changeScene("gamePart")
    })

    sock.on('sendVote',(text,answers,voteN)=>{
        game.question=text
        game.answers=answers
        game.voteN=voteN

        console.log(text)
        console.log(answers)


        changeScene("gameVote")
    })

    sock.on('ackVote',(status,voteLeft)=>{
        if(status=="ok") {
            if(voteLeft==0)
                changeScene('wait')
        }
        game.canInteract=true
    })

    sock.on('sendScore',(scores)=>{
        for(let p of game.players) {
            for(let s of scores) {
                if(p.name==s.name) {
                    p.score=s.score
                }
            }
        }
    })

    sock.on('errorMsg',(text)=>{
        setError(text)
        console.error(text)
    })
}

export function clientStartGame() {
    sock.emit('returnReady')
}

export function clientReturnParts() {
    game.partsVal.push(findObject("answer").text+"")
    if(game.partsVal.length==game.parts.length) {
        sock.emit('returnParts',game.partsVal)
        changeScene('wait')
        return
    }
    changeScene('gamePart',true)
}

export function clientReturnAnswer() {
    const val = findObject("answer").text+""
    if(!val||!val.length) {
        setError("Answer is empty!")
        return;
    }

    game.returnAnswers.push(val)
    if(game.returnAnswers.length==game.questions.length) {
        sock.emit('returnAnswers',game.returnAnswers)
        changeScene('wait')
        return;
    }
    changeScene('gameAnswer',true)
}

export function clientReturnVote(ind) {
    sock.emit('returnVote', ind)
    game.canInteract=false
}
