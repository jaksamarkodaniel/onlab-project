//import * as PIXI from "./pixi.mjs";
import {app, game, findObject, getOrientation} from "./gameHelper.mjs"
import {changeScene, scene} from "./gameScene.mjs"
import {loadTextures} from "./gameLoader.mjs"

loadTextures(() => {
    document.body.appendChild(app.view);

    changeScene("login")
    resizeEvent()
})


//Click out of input
app.renderer.view.addEventListener('pointerdown',()=>{
    findObject("uname")?.blur()
    findObject("code")?.blur()

    findObject("answer")?.blur()
})

window.addEventListener("resize",resizeEvent)

function resizeEvent() {
    let w = window.outerWidth, h = window.outerHeight
    if(w>1280&&getOrientation().indexOf("portrait")==-1) {
        h=h/w*1280
        w=1280
    }

    let scaleFactor = Math.min(
        w / game.w(),
        h / game.h()
    );

    app.renderer.view.style.width = `${game.w()*scaleFactor}px`;
    app.renderer.view.style.height = `${game.h()*scaleFactor}px`;
    app.renderer.resize(game.w()*scaleFactor, game.h()*scaleFactor);
    app.stage.scale.set(scaleFactor)


    if(game.orientation!=getOrientation()) {
        game.orientation=getOrientation()
        changeScene(scene.current,true)
    }
    console.log(game.w()+" "+game.h())
}

app.ticker.add(() => {
    let err = findObject("errorMsg")
    if(err)
        err.alpha-=.025
});
