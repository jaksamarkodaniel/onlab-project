import * as express from "express";
import { QuestionHandler } from "./controllers/QuestionHandler";
import { SocketHandler } from "./controllers/SocketHandler";
import compress from "compression";


require('dotenv').config()

const app = express.default();
app.set("port", process.env.PORT || 3000);
app.use(compress())

const http = require("http").Server(app);
const socketHandler = new SocketHandler(http)

//Pre-import sheet
QuestionHandler.instance()

app.use('/', express.static('public'))

const server = http.listen(3000, function() {
  console.log("listening on *:3000");
});

setInterval(function(){
  socketHandler.loop()
},(1000/30))