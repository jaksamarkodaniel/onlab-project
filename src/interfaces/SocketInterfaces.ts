export enum ConnType {
    Join="Join",
    Create="Create"
}

export enum SocketType {
    Player="Player",
    Spectator="Spectator",
    Admin="Admin"
}

export interface ServerToClientEvents {
    sendPlayers: (dat:SocketDataSafe[]) => void;

    //Game stuffs
    sendParts: (text:string[]) => void;
    sendQuestions: (questions: QuestionData[]) => void;
    sendVote: (question:string, answers: string[], voteN: number) => void;
    sendScore: (players: PlayerData[]) => void;

    ackVote: (status:string,voteLeft:number) => void;

    //Minigame stuffs
    updateAll: (players: MiniPlayerData[]) => void;

    errorMsg: (text: string) => void;
}

export interface ClientToServerEvents {
    returnReady: () => void;

    //Game stuffs question
    returnParts: (text: string[]) => void;
    returnAnswers: (text: string[]) => void;
    returnVote: (id: number) => void;

    //minigame
    updatePos: (data: MiniPlayerData) => void;

    join: (name: string, type: string) => void;
}

export interface InterServerEvents {
    ping: () => void;
}

export interface QuestionData {
    text: string,
    parts: string[]
}

export interface SocketDataSafe {
    name: string;
    type: SocketType;
    pos: number[];
    ind: number;
}

export interface PlayerData {
    name: string,
    score: number
}

export interface MiniPlayerData {
    ind: number;
    pos: number[];
}

export interface SocketData extends SocketDataSafe, PlayerData {
    voteLeft: number;
}