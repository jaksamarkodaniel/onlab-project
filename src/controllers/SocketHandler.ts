import { Server } from "socket.io";
import { Lobby } from "../models/Lobby";
import { Player } from "../models/Player";
import { ClientToServerEvents, ConnType, InterServerEvents, ServerToClientEvents, SocketData, SocketType } from "../interfaces/SocketInterfaces";

export class SocketHandler {
    io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, SocketData>;
    lobbies: Lobby[];

    constructor(http: any) {
        this.lobbies = [];
        this.io = require("socket.io")(http);

        this.io.use((socket,next)=>{
            //Potential middleware
            next();
        })

        this.io.on("connection", (sock) => {
            const query = sock.handshake.query;
            console.log(query.name)
            if(query.type==SocketType.Player) {
                let lobby: Lobby | null;
                switch(query.connType) {
                    case ConnType.Create:
                            const player = new Player(sock,query.name as string);
                            let code = '';
                            do {
                                code = this.zeroPad(~~(Math.random()*9999),4)
                            } while(this.lobbyExists(code))

                            lobby = new Lobby(this,player,code)
                            this.lobbies.push(lobby);
                            if(process.env.DEBUG)
                                console.log("Player "+player.socket.data.name+" created lobby "+lobby.code)
                        break;
                    case ConnType.Join:
                            lobby = this.lobbyExists(query.code as string)
                            if(lobby!=null) {
                                const player = new Player(sock,query.name as string);
                                lobby.addPlayer(player)
                                if(process.env.DEBUG) {
                                    if(lobby!=null) {
                                        console.log("Player "+query.name+" joined to lobby "+lobby.code)
                                    }
                                }
                            } else {
                                sock.emit("errorMsg","Lobby does not exists")
                            }
                        break;
                }
            }
        })
    }

    private zeroPad (num:number, places:number) {
        return String(num).padStart(places, '0')
    }

    lobbyDelete(code:string): void {
        this.lobbies.splice(this.lobbies.indexOf(this.lobbyExists(code)!!))
        console.log("Lobby("+code+") is destroyed")
    }

    lobbyExists(code:string): Lobby | null {
        for(let l of this.lobbies) {
            if(code==l.code)
                return l;
        }
        return null;
    }

    loop() {
        for(let lobby of this.lobbies) {
            lobby.loop()
        }
    }
}