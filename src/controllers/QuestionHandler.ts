import { SheetHandler } from "./SheetHandler"

export class QuestionHandler {
    public static _instance: QuestionHandler;
    questions: string[] = []
    ind: number;
    private constructor() {
        this.ind=0

        const sheetHandler = SheetHandler.instance((data:any)=>{
            this.questions = data.data.values
            this.questions.shift()
            this.shuffleQuestions()
        })          
    }

    private shuffleQuestions() {
        const array = this.questions
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    public static instance() {
        if(!this._instance) {
            this._instance = new QuestionHandler()
        }
        return this._instance
    }

    public static getQuestion(): string {
        const inst = this.instance()
        const q = inst.questions[inst.ind++]
        if(inst.ind==inst.questions.length) {
            inst.ind=0;
            inst.shuffleQuestions()
        }
        return q;
    }
}