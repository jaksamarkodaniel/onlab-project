import { Player } from "../models/Player"

class GameObjects {
    pos: number

    constructor(pos:number) {
        this.pos=pos
    }
}

export class GameHandler {
    players: Player[]
    objects: GameObjects[]
    constructor(players:Player[]) {
        this.players = players

        for(const p of this.players) {
            p.socket.on("updatePos",(data)=>{
                p.data.pos=data.pos
            })
        }

        this.objects = []
    }

    spawnObject(pos: number) {
        this.objects.push(new GameObjects(pos))
    }

    loop() {

    }
}