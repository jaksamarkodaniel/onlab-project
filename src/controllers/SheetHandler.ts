import { GaxiosResponse } from "gaxios";
import { Compute, GoogleAuth } from "google-auth-library";
import { JSONClient } from "google-auth-library/build/src/auth/googleauth";
import {google, sheets_v4} from "googleapis"

export class SheetHandler {
    private static spreadSheetId = "1gLClytCeTwxU-_snla50fXQ5nszKeZcABPqKU_KUCmM"

    private static _instance: SheetHandler;
    auth: GoogleAuth<JSONClient>;
    client: JSONClient | Compute | undefined;
    googleSheet: sheets_v4.Sheets | undefined;


    private constructor(fn: any) {
        this.auth = new google.auth.GoogleAuth({
            keyFile: "./credentials.json",
            scopes: "https://www.googleapis.com/auth/spreadsheets"
        });

        this.init().then(()=>{ 
            this.getData(fn) 
        })
    }

    private async init() {
        this.client = await this.auth.getClient()
        this.googleSheet = google.sheets({version:"v4", auth: this.client})
    }

    public async getData(fn: ((value: GaxiosResponse<sheets_v4.Schema$ValueRange>) => GaxiosResponse<sheets_v4.Schema$ValueRange> | PromiseLike<GaxiosResponse<sheets_v4.Schema$ValueRange>>) | null | undefined) {
        const inst = SheetHandler._instance
        return inst.googleSheet!!.spreadsheets.values.get({
            auth: inst.auth,
            spreadsheetId:SheetHandler.spreadSheetId,
            range:"Munkalap1"
        }).then(fn);
    }

    public static instance(fn: any): SheetHandler {
        if(!SheetHandler._instance)
            SheetHandler._instance = new SheetHandler(fn)
        else
            fn()
        
        return SheetHandler._instance
    }
}