import { QuestionHandler } from "../controllers/QuestionHandler";
import { SocketHandler } from "../controllers/SocketHandler";
import { MiniPlayerData, PlayerData, QuestionData, SocketDataSafe } from "../interfaces/SocketInterfaces";
import { Player } from "./Player";
import { Question } from "./Question";

export enum States {
    Wait,
    Part,
    Question,
    Vote,
    RoundEnd
}
export class Lobby {
    static ROUND_N = 1
    static MAX_P = 6
    
    code: string;
    state: States;
    handler: SocketHandler;
    players: Player[];
    round: number;

    qInd: number;
    questions: Question[];
    objectives: string[];

    paused: boolean
    

    constructor(handler:SocketHandler,firstPlayer:Player,code:string) {
        this.code = code
        
        this.handler=handler
        this.players = []

        this.addPlayer(firstPlayer)
        this.state = States.Wait

        /**
         * -5 minigame result
         * -4 minigame run
         * -3 minigame start
         * -2 lobby
         * -1 parts
         * 0 answers
         * 1..ROUND_N votes
         */
        this.round = -2
        this.paused = false

        this.qInd = 0
        this.questions = []
        this.objectives = []
    }

    public get isFull(): boolean {
        return this.players.length==Lobby.MAX_P
    }

    public addPlayer(p:Player): boolean {
        if(!this.isFull) {
            if(this.nameExists(p.data.name)) {
                p.socket.emit('errorMsg',"Name already in use!")
                return false;
            }
            let ind = -1;
            do {
                ind = ~~(Math.random()*6)
            } while(this.players.map(p=>p.socket.data.ind).indexOf(ind)!=-1)
            p.socket.data.ind=ind

            this.players.push(p)
            p.socket.join(this.code)

            console.log("Players in lobby("+this.code+"):"+this.players.length)

            p.socket.on("returnReady",()=>{
                if(this.round==-2)
                    this.nextRound()
            })

            p.socket.on("returnParts", (parts)=>{
                if(parts.length!=this.questions.length) {
                    p.socket.emit('errorMsg',"Not enough parts!")
                    return;
                }
                for(let i=0;i<parts.length;i++)
                    this.questions[i].parts.push({p,part:parts[i]})
                if(this.questions[0].parts.length==this.players.length) {
                    this.nextRound()
                }
            })

            p.socket.on("returnAnswers",(texts)=>{
                if(texts.length!=this.questions.length) {
                    p.socket.emit("errorMsg","Wrong amount of answers from "+p.data.name+"!")
                    return;
                }
                console.log(texts)
                for(let ind in this.questions) {
                    this.questions[ind].addAnswer(texts[ind],p)
                }
                if(this.round==0) {
                    if(this.questions[0].answered)
                        this.nextRound()
                }
            })
            p.socket.on("returnVote",(id)=>{
                let status = "ok"
                if(!this.questions[this.qInd-1].addVote(id,p))
                    status = "no"
                
                p.socket.emit('ackVote',status,p.data.voteLeft)
                if(this.questions[this.qInd-1].allVoted)
                    this.nextRound()
            })

            this.handler.io.to(this.code).emit("sendPlayers",
                this.players.map(v=>{
                    return v.data as SocketDataSafe
                }
            ))

            p.socket.on("disconnect",()=>{
                this.players.splice(this.players.indexOf(p),1)
                this.paused=true
                if(this.players.length==0) {
                    this.handler.lobbyDelete(this.code)
                    return;
                }
                console.log("Players in lobby("+this.code+"):"+this.players.length)
            })

            return true;
        }
        //Kick out
        p.socket.emit('errorMsg',"Lobby is full!")
        p.socket.disconnect(true)
        return false;
    }

    nextRound() {
        this.round++
        switch(this.round) {
            case -1:
                this.makeQuestions(Lobby.ROUND_N)
                this.sendPart(this.objectives)
                break;
            case 0:
                this.sendQuestions(this.questions)
                break;
            case 1+Lobby.ROUND_N:
                break;
            default:
                this.sendVote(this.questions[this.qInd++])
        }
    }

    public kickPlayer(p: Player) {
        const ind = this.players.findIndex(_p=>_p==p)
        p.socket.disconnect(true)
        this.players.splice(ind,1)
    }

    thisRoom() {
        return this.handler.io.to(this.code)
    }

    //Game stuffs
    sendQuestions(questions: Question[]) {
        const qDats: QuestionData[] = []
        for(let q of questions) {
            qDats.push({text: q.text,
                parts: q.parts.map(p=>p.part)
            })
        }
        this.thisRoom().emit("sendQuestions", qDats)
    }

    sendPart(partTypes: string[]) {
        this.thisRoom().emit("sendParts",partTypes)
    }

    sendVote(question: Question) {
        this.thisRoom().emit("sendVote",question.text,question.listAnswers(),1)
    }

    sendScore() {
        this.thisRoom().emit("sendScore",
            this.players.map(p=>{return {name:p.data.name,score:p.data.score}}))
    }


    nameExists(name:string): boolean {
        return this.players.map(p=>p.data.name).indexOf(name)!=-1
    }

    private makeQuestions(qN: number) {
        for(let i=0;i<qN;i++) {
            const question = new Question(QuestionHandler.getQuestion(),this.players.length,2)
            this.objectives.push(question.objective)
            this.questions.push(question)
        }
    }
    loop() {
        if(this.round==-4) {
            const dat = this.players.map((p)=>{return <MiniPlayerData>{pos:p.data.pos,ind:p.data.ind}})
            this.thisRoom().emit("updateAll",dat)
        }
    }
}