import { Socket } from "socket.io";
import { ClientToServerEvents, InterServerEvents, ServerToClientEvents, SocketData } from "../interfaces/SocketInterfaces";
import { GameData } from "./GameData";

export class Player {
    _socket: Socket<ClientToServerEvents,ServerToClientEvents,InterServerEvents,SocketData>;
    //gameData: GameData

    constructor(sock: Socket, name: string) {
        this._socket=sock;

        let lobbyX = Math.random()*.75, lobbyY = Math.random()*.75;

        Object.assign(sock.data,{
            name: name,
            
            //Game stuffs
            score: 0,
            pos: [lobbyX,lobbyY],
            ind: -1
        })
    }

    public get data() {
        return <SocketData>this.socket.data
    }

    public get socket() {
        return this._socket;
    }
}