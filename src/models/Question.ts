import { Player } from "./Player"

class Answer {
    text: string
    votes: Player[]
    owner: Player
    constructor(text:string, owner: Player) {
        this.text=text;
        this.votes=[];
        this.owner = owner;
    }

    public vote(p: Player) {
        if(p!=this.owner) {
            this.votes.push(p)
            return true
        }
        return false
    }
}

export class Question {
    text: string
    answers: Answer[]
    maxNum: number
    voteCount: number
    votePerPlayer: number
    objective: string
    parts: {part:string,p:Player}[]

    constructor(text:string, maxNum: number, votePerPlayer: number) {
        const reg= new RegExp(/\[(.*)\]/)
        this.answers = []
        this.maxNum = maxNum
        this.votePerPlayer = votePerPlayer
        this.voteCount = 0

        this.objective = (text.toString().match(reg)||[""])[0]
        this.parts = []
        this.text=text.toString().replace(reg,"*")
    }

    public get answered() {
        return this.answers.length==this.maxNum
    }

    public addAnswer(text: string, owner: Player) {
        if(this.findAnswerByOwner(owner)==-1) {
            this.answers.push(new Answer(text,owner))
            if(this.answers.length==this.maxNum)
                return true
        }
        return false
    }

    public listAnswers() {
        let ret=[]
        for(const a of this.answers)
            ret.push(a.text)
        return ret;
    }
    /**
     * 
     * @param id The index of the answer
     * @param p Player instance
     * @returns True if it was successful
     */
    addVote(id:number, p: Player) {
        for(let a of this.answers)
            if(a.votes.findIndex(val=>val==p)!=-1)
                return false
        
        if(this.answers[id].vote(p))
            this.voteCount++
        return this.voteCount==this.maxNum
    }

    public get allVoted() {
        return this.voteCount==this.maxNum * this.votePerPlayer
    }

    findAnswerByOwner(p: Player): number {
        return this.answers.findIndex(val=>val.owner==p)
    }
}